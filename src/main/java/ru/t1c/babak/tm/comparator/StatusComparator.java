package ru.t1c.babak.tm.comparator;

import ru.t1c.babak.tm.api.model.IHaveStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHaveStatus> {

    INSTANCE;

    @Override
    public int compare(final IHaveStatus o1, final IHaveStatus o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getStatus() == null || o2.getStatus() == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
