package ru.t1c.babak.tm.comparator;

import ru.t1c.babak.tm.api.model.IHaveName;

import java.util.Comparator;

public enum NameComparator implements Comparator<IHaveName> {

    INSTANCE;

    @Override
    public int compare(final IHaveName o1, final IHaveName o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getName() == null || o2.getName() == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
