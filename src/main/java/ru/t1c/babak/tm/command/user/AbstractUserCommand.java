package ru.t1c.babak.tm.command.user;

import ru.t1c.babak.tm.api.service.IAuthService;
import ru.t1c.babak.tm.api.service.IUserService;
import ru.t1c.babak.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

//    /**
//     * Render all Users in default order.
//     */
//    void renderAllUsers() {
//        final List<User> Users = getUserService().findAll();
//        renderUsers(Users);
//    }
//
//    void renderUsers(final List<User> Users) {
//        for (int i = 0; i < Users.size(); ) {
//            final User User = Users.get(i++);
//            if (User == null) continue;
//            System.out.println("\t" + i + ". " + User);
//        }
//    }
//
//    void showUser(final User User) {
//        showWbs(User);
//    }

}
