package ru.t1c.babak.tm.command.task;

import ru.t1c.babak.tm.util.TerminalUtil;

public final class TaskAddToProjectByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-add-to-project-by-id";

    public static final String DESCRIPTION = "Relate task to project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("\tENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("\tENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getServiceLocator().getProjectTaskService().bindTaskToProject(projectId, taskId);
    }

}
