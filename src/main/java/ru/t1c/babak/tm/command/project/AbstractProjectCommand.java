package ru.t1c.babak.tm.command.project;

import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.command.AbstractCommand;
import ru.t1c.babak.tm.model.Project;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    /**
     * Render all projects in default order.
     */
    void renderAllProjects() {
        final List<Project> projects = getProjectService().findAll();
        renderProjects(projects);
    }

    void renderProjects(final List<Project> projects) {
        for (int i = 0; i < projects.size(); ) {
            final Project project = projects.get(i++);
            if (project == null) continue;
            System.out.println("\t" + i + ". " + project);
        }
    }

    void showProject(final Project project) {
        showWbs(project);
    }

}
