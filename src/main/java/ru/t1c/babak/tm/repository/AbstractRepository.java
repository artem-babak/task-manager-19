package ru.t1c.babak.tm.repository;

import ru.t1c.babak.tm.api.repository.IRepository;
import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.model.AbstractModel;

import java.util.*;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M add(M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(Comparator comparator) {
        final List<M> result = new ArrayList<>(models);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @Override
    public M findOneById(String id) {
        for (final M model : models) {
            if (id.equals(model.getId())) return model;
        }
        return null;
    }

    @Override
    public M findOneByIndex(Integer index) {
        return models.get(index);
    }

    @Override
    public boolean existsById(String id) {
        return findOneById(id) != null;
    }

    @Override
    public M remove(M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(Integer index) {
        final M model = findOneByIndex(index);
        return remove(model);
    }

    @Override
    public void clear() {
        models.clear();
    }

}
