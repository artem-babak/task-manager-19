package ru.t1c.babak.tm.repository;

import ru.t1c.babak.tm.api.repository.IUserRepository;
import ru.t1c.babak.tm.enumerated.Role;
import ru.t1c.babak.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User create(String login) {
        final User user = new User();
        user.setLogin(login);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash, String email) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        return add(user);
    }

    @Override
    public User create(String login, String passwordHash, Role role) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        return add(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User setPasswordHash(final User user, final String passwordHash) {
        if (user == null) return null;
        user.setPasswordHash(passwordHash);
        return user;
    }

    @Override
    public User setPasswordHashById(final String id, final String passwordHash) {
        final User user = findOneById(id);
        if (user == null) return null;
        setPasswordHash(user, passwordHash);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for (final User user : models) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (final User user : models) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }


}
