package ru.t1c.babak.tm.exception.auth;

import ru.t1c.babak.tm.exception.entity.AbstractEntityNotFoundException;

public final class InvalidCredentialException extends AbstractEntityNotFoundException {

    public InvalidCredentialException() {
        super("Error! Invalid user credential...");
    }

}
