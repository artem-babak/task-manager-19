package ru.t1c.babak.tm.exception.field;

public final class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

}
