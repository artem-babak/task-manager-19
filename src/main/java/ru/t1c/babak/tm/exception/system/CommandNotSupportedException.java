package ru.t1c.babak.tm.exception.system;

import ru.t1c.babak.tm.exception.AbstractException;

public final class CommandNotSupportedException extends AbstractException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! Command ``" + command + "`` not supported...");
    }

}
