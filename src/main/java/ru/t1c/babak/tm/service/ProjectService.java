package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.IProjectRepository;
import ru.t1c.babak.tm.api.service.IProjectService;
import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.exception.entity.ProjectNotFoundException;
import ru.t1c.babak.tm.exception.field.*;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.repository.AbstractRepository;
import ru.t1c.babak.tm.repository.ProjectRepository;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {


    public ProjectService(IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }


    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(String name, String description, Date dateBegin, Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        if (status == null) throw new StatusIncorrectException();
        project.setStatus(status);
        return project;
    }

}
