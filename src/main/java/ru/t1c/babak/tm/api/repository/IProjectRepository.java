package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
