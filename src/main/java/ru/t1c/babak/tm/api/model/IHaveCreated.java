package ru.t1c.babak.tm.api.model;

import java.util.Date;

public interface IHaveCreated {

    Date getCreated();

    void setCreated(Date created);

}