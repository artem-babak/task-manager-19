package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.api.repository.IRepository;
import ru.t1c.babak.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
