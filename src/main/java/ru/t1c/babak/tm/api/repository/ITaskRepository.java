package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    Task create(String name);

    Task create(String name, String description);

}
