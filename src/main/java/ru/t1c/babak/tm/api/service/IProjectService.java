package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);


    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);


    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);


}
