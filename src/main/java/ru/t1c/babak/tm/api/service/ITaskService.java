package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.enumerated.Sort;
import ru.t1c.babak.tm.enumerated.Status;
import ru.t1c.babak.tm.model.Project;
import ru.t1c.babak.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {


    List<Task> findAllByProjectId(String projectId);


    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);


    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);


    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
