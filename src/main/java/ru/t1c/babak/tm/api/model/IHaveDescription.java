package ru.t1c.babak.tm.api.model;

public interface IHaveDescription {

    String getDescription();

    void setDescription(String description);

}